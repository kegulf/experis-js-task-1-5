/**
 * Task 3 - JavaScript Clock
 * -------------------------
 * Build a JavaScript Clock
 * Display the current time and date on a webpage
 * Use moment js
 * Install it with npm
 * Use Webpack starter (Includes Moment)
 * https://github.com/sumodevelopment/webpack-starter.git
 * https://momentjs.com/
 * Be creative add an image
 */

const width = 400,
	height = 400;

const task3 = () => {
	const canvas = createCanvas();
	const ctx = canvas.getContext("2d");

	setInterval(() => updateClock(ctx), 500);

	return canvas;
};

const updateClock = ctx => {
	wipeCanvas(ctx);
	drawClockFace(ctx);
	drawNumbers(ctx);
	drawNumberLines(ctx);

	const now = new Date();
	const hour = now.getHours();
	const minutes = now.getMinutes();
	const seconds = now.getSeconds();

	drawHourArm(ctx, hour);
	drawMinutesArm(ctx, minutes);
	drawSecondsArm(ctx, seconds);

	// Draw center dot
	ctx.beginPath();
	ctx.arc(width / 2, height / 2, 5, 0, 2 * Math.PI);
	ctx.fillStyle = "black";
	ctx.fill();
};

const drawHourArm = (ctx, hour) => {
	const coords = angleToPolarCoordinates(width * 0.2, (hour * Math.PI) / 6);

	drawLine(ctx, { x: width / 2, y: height / 2 }, coords, "black", 5);
};

const drawMinutesArm = (ctx, minutes) => {
	const coords = angleToPolarCoordinates(width * 0.25, (minutes * Math.PI) / 30);

	drawLine(ctx, { x: width / 2, y: height / 2 }, coords, "green", 3);
};

const drawSecondsArm = (ctx, seconds) => {
	const coords = angleToPolarCoordinates(width * 0.28, (seconds * Math.PI) / 30);

	drawLine(ctx, { x: width / 2, y: height / 2 }, coords, "red", 2);
};

const wipeCanvas = ctx => {
	ctx.beginPath();
	ctx.fillStyle = "white";
	ctx.rect(0, 0, width, height);
	ctx.fill();
};

const drawClockFace = ctx => {
	ctx.beginPath();
	ctx.arc(width / 2, height / 2, width * 0.4, 0, Math.PI * 2);
	ctx.strokeStyle = "black";
	ctx.stroke();
};

const drawNumbers = ctx => {
	for (let i = 1; i <= 12; i++) {
		const { x, y } = angleToPolarCoordinates(width * 0.3, (i * Math.PI) / 6);

		y += 8;
		ctx.beginPath();
		ctx.font = "24px Arial";
		ctx.textAlign = "center";
		ctx.fillStyle = "black";
		ctx.fillText(i, x, y);
	}
};

const drawNumberLines = ctx => {
	for (let i = 0; i < 60; i++) {
		const innerRadius = i % 5 === 0 ? width * 0.34 : width * 0.36;

		const outer = angleToPolarCoordinates(width * 0.38, (i * Math.PI) / 30);
		const inner = angleToPolarCoordinates(innerRadius, (i * Math.PI) / 30);

		drawLine(ctx, outer, inner, "black", i % 5 === 0 ? 3 : 1);
	}
};

const drawLine = (ctx, from, to, color, width) => {
	ctx.beginPath();
	ctx.moveTo(from.x, from.y);
	ctx.lineTo(to.x, to.y);
	ctx.lineWidth = width;
	ctx.strokeStyle = color;
	ctx.stroke();
};

const angleToPolarCoordinates = (radius, angle) => {
	// Offset of 90 degrees anti-clock wise to make i be relative to 12 on the clockface
	// And offset of 300 px to make it focused around center-canvas.
	return {
		x: radius * Math.cos(angle - Math.PI / 2) + width / 2,
		y: radius * Math.sin(angle - Math.PI / 2) + height / 2
	};
};

const createCanvas = () => {
	const canvas = document.createElement("canvas");

	canvas.width = width;
	canvas.height = height;
	canvas.style.border = "1px solid black";

	return canvas;
};

export default task3;
