import "./task5.scss";

const ROCK = "rock",
	PAPER = "paper",
	SCISSORS = "scissors";

const choices = [ROCK, PAPER, SCISSORS];

const task5 = () => {
	createChoiceButtons();
};

const createChoiceButtons = () => {
	const buttonPanel = document.querySelector("#button-panel");

	choices.forEach(choice => {
		const button = document.createElement("button");
		button.className = "choice-button";
		button.id = capitalizeFirstLetter(choice);
		button.innerHTML = capitalizeFirstLetter(choice);
		button.onclick = startRound;
		buttonPanel.appendChild(button);
	});

	return buttonPanel;
};

const startRound = e => {
	const outputContainer = document.querySelector("#output-container");
	const userChoice = e.target.innerHTML.toLowerCase();

	let counter = 0;

	const randIndex = Math.floor(Math.random() * 3);
	const cpuChoice = choices[randIndex];

	document.querySelectorAll(".choice-button").forEach(btn => {
		btn.disabled = true;
	});

	const interval = setInterval(() => {
		outputContainer.innerHTML = "";
		const p = document.createElement("p");
		p.className = "increasing-text-size";
		outputContainer.appendChild(p);

		if (counter < 3) {
			p.innerHTML = capitalizeFirstLetter(choices[counter++]);
		} else {
			clearInterval(interval);
			const result = getResultOfRound(userChoice, cpuChoice);

			p.innerHTML = `(You) ${capitalizeFirstLetter(userChoice)} - 
			${capitalizeFirstLetter(cpuChoice)} (CPU)<br><br> ${result}`;

			document.querySelectorAll(".choice-button").forEach(btn => {
				btn.disabled = false;
			});
		}
	}, 900);
};

const getResultOfRound = (userChoice, cpuChoice) => {
	if (userChoice === cpuChoice) {
		return "It's a draw";
	}

	if (userChoice === PAPER) {
		if (cpuChoice === ROCK) return "You win!";
		if (cpuChoice === SCISSORS) return "CPU wins";
	} else if (userChoice === ROCK) {
		if (cpuChoice === SCISSORS) return "You win!";
		if (cpuChoice === PAPER) return "CPU wins";
	} else if (userChoice === SCISSORS) {
		if (cpuChoice === PAPER) return "You win!";
		if (cpuChoice === ROCK) return "CPU wins";
	}
};

const capitalizeFirstLetter = string => {
	return string.substring(0, 1).toUpperCase() + string.substring(1, string.length);
};
export default task5;
