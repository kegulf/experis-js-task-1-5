/**
 * Task 2 - Build a calculator (Group Task - 2 per group)
 * Build a calculator using JavaScript and HTML
 * Use buttons for numbers and display the results on a web page
 * The user should be able to add, subtract, divide and multiply.
 * - Check for rounding
 * - Make it pretty-ish
 */

import "./task4.scss";

const calculator = {
	currentResult: 0,
	currentString: "",
	lastInputWasOperand: false,
	lastInputWasEquals: false
};

const createOutputArea = () => {
	const outputWrapper = document.createElement("div");
	outputWrapper.id = "output-wrapper";

	const outputCalculationString = document.createElement("span");
	outputCalculationString.id = "output-calculation-string";
	outputWrapper.appendChild(outputCalculationString);

	const outputField = document.createElement("p");
	outputField.id = "output-field";
	outputField.innerHTML = "0";
	outputWrapper.appendChild(outputField);

	return outputWrapper;
};

const task4 = () => {
	const calculatorDiv = document.createElement("div");
	calculatorDiv.id = "calculator";

	calculatorDiv.appendChild(createOutputArea());

	// Add number buttons
	for (let i = 0; i <= 9; i++) {
		let className = i === 0 ? "number-btn btn-0" : "number-btn";

		calculatorDiv.appendChild(createButton(className, `btn-${i}`, i, handleNumberInput));
	}

	for (let i = 0; i < functionalButtons.length; i++) {
		let btn = functionalButtons[i];

		calculatorDiv.appendChild(
			createButton(btn.className, btn.gridArea, btn.text, btn.onclick)
		);
	}

	return calculatorDiv;
};

const handleNumberInput = e => {
	const value = e.target.innerHTML;

	const outputField = document.querySelector("#output-field");

	if (calculator.lastInputWasOperand || (outputField.innerHTML === "0" && value !== ".")) {
		outputField.innerHTML = "";
		calculator.lastInputWasOperand = false;
		calculator.lastInputWasEquals = false;
	}

	outputField.innerHTML += value;
};

const resetCalculator = () => {
	document.querySelector("#output-field").innerHTML = "0";
	document.querySelector("#output-calculation-string").innerHTML = "";

	calculator.currentResult = 0;
	calculator.currentString = "";
	calculator.lastInputWasOperand = false;
	calculator.lastInputWasEquals = false;
};

const removeLast = () => {
	const out = document.querySelector("#output-field");

	out.innerHTML = out.innerHTML.slice(0, -1);

	if (out.innerHTML.length == 0) {
		out.innerHTML = "0";
	}
};

const divide = () => {
	if (!calculator.lastInputWasOperand) {
		updateCalculatorAfterOperandInput("/");
	}
};
const multiply = () => {
	if (!calculator.lastInputWasOperand) {
		updateCalculatorAfterOperandInput("*");
	}
};
const subtract = () => {
	if (!calculator.lastInputWasOperand) {
		updateCalculatorAfterOperandInput("-");
	}
};
const add = () => {
	if (!calculator.lastInputWasOperand) {
		updateCalculatorAfterOperandInput("+");
	}
};

const equals = () => {
	const out = document.querySelector("#output-field");

	if (
		!(calculator.lastInputWasOperand || calculator.lastInputWasEquals) &&
		out.innerHTML !== "0"
	) {
		calculator.lastInputWasEquals = true;
		calculator.currentString += out.innerHTML + " ";
		console.log(calculator.currentString);
		document.querySelector("#output-calculation-string").innerHTML = calculator.currentString;
		document.querySelector("#output-field").innerHTML = eval(calculator.currentString);
	}
};

const updateCalculatorAfterOperandInput = operand => {
	const currentValue = document.querySelector("#output-field").innerHTML;

	if (!calculator.lastInputWasEquals && currentValue !== "0") {
		calculator.currentString += `${currentValue} ${operand} `;
	} else {
		calculator.currentString += `${operand} `;
	}

	let resultString = getFormatedCurrentString();

	calculator.lastInputWasOperand = true;
	calculator.lastInputWasEquals = false;

	document.querySelector("#output-calculation-string").innerHTML = resultString;
	document.querySelector("#output-field").innerHTML = calculator.currentResult;
};

const getFormatedCurrentString = () => {
	let returnString = calculator.currentString;

	if (calculator.currentString.length > 33) {
		const stringArray = calculator.currentString.split(" ");

		// remove the first element to reduce the string length
		stringArray.shift();

		returnString = stringArray.join(" ");
		returnString = ".. " + returnString;
	}
	return returnString;
};

const createButton = (className, gridArea, value, onclick) => {
	const btn = document.createElement("button");

	btn.innerHTML = value;
	btn.onclick = onclick;
	btn.className = className;
	btn.style.gridArea = gridArea;

	return btn;
};

const functionalButtons = [
	{
		text: "CLR",
		className: "clear-all",
		gridArea: "clearall",
		onclick: resetCalculator
	},

	{
		text: "&larr;",
		className: "backspace",
		gridArea: "backspace",
		onclick: removeLast
	},

	{
		text: "&divide;",
		className: "operand",
		gridArea: "divide",
		onclick: divide
	},
	{
		text: "&times;",
		className: "operand",
		gridArea: "times",
		onclick: multiply
	},

	{
		text: "-",
		className: "operand",
		gridArea: "minus",
		onclick: subtract
	},

	{
		text: "+",
		className: "operand",
		gridArea: "plus",
		onclick: add
	},
	{
		text: "=",
		className: "operand equals",
		gridArea: "equals",
		onclick: equals
	},
	{
		text: ".",
		className: "number-btn",
		gridArea: "btn-dot",
		onclick: handleNumberInput
	}
];

export default task4;
