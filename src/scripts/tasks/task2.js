/**
 * Task 2 - FizzBuzz
 * Write a small program to display the words Fizz and Buzz
 * Write a loop up to 50
 * If the current loop index is divisible by 3 - Display Fizz
 * If the current loop index is divisible by 5 - Display Buzz
 * If the current loop index is divisible by 15 - Display FizzBuzz
 */

const task2 = () => {
	let returnElement = document.createElement("p");

	for (let i = 0; i < 50; i++) {
		if (i % 15 === 0) {
			returnElement.innerHTML += `${i}: FizzBuzz<br>`;
		} else if (i % 3 === 0) {
			returnElement.innerHTML += `${i}: Fizz<br>`;
		} else if (i % 5 === 0) {
			returnElement.innerHTML += `${i}: Buzz<br>`;
		}
	}

	return returnElement;
};

export default task2;
