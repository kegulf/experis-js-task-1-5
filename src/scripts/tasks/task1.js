/*
	Task 1 - Math stuff
	----------------------

	• Write a small program to display the first 50 numbers in
	the fibonacci sequence
	• Here is how it works:
	• https://www.mathsisfun.com/numbers/fibonaccisequence.html
	• TIPS:
	• Use a while or for loop
*/

const task1 = () => {
	const fibonacciNumbers = [1, 1];

	let fib1 = 1,
		fib2 = 1;

	while (fibonacciNumbers.length < 50) {
		const sum = fib1 + fib2;

		fib1 = fib2;
		fib2 = sum;

		fibonacciNumbers.push(sum);
	}

	const returnElement = document.createElement("p");
	returnElement.innerHTML = fibonacciNumbers.join(", ");
	return returnElement;
};

export default task1;
