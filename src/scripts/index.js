import "./../styles/index.scss";

import * as moment from "moment";

console.log(moment().format("LLL"));

import { task1, task2, task3, task4, task5 } from "./tasks";

/**
 * For more information on using Moment head to https://momentjs.com/
 */

window.onload = () => {
	document.querySelector("#task-1-output").appendChild(task1());
	document.querySelector("#task-2-output").appendChild(task2());

	document.querySelector("#task-3-output").appendChild(task3());
	document.querySelector("#task-4-output").appendChild(task4());
	task5();

	for (let i = 1; i <= 5; i++) {
		document.querySelector(`#task-${i}-button`).onclick = event => {
			toggleTaskContainer(event, `task-${i}-container`);
		};
	}
};

const toggleTaskContainer = (event, containerId) => {
	const button = event.target;
	const container = document.querySelector(`#${containerId}`);

	if (button.classList.contains("closed")) {
		button.classList.remove("closed");
		button.classList.add("open");

		container.classList.remove("hidden");
		container.classList.add("visible");
	} else {
		button.classList.remove("open");
		button.classList.add("closed");

		container.classList.remove("visible");
		container.classList.add("hidden");
	}
};
